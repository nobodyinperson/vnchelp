% vnchelp(1) | simple remote VNC help

NAME
====


**vnchelp** - a simple tool for remote VNC help

SYNOPSIS
========

see vnchelp -h

FILES
=====

|   File                      | Purpose                                                           |
|-----------------------------|-------------------------------------------------------------------|
|`~/.cache/vnchelp/config.cfg`| configuration file with saved parameters from last successful run |


AUTHOR
======


Yann Büchau <nobodyinperson@gmx.de>



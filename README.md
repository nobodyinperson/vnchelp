> # Disclaimer
> 
> Use this software at your own risk. I am not responsible for any security 
> breaches. You have been warned.
> 
> However, I have been using this software myself without issues.

[![pipeline status](https://gitlab.com/nobodyinperson/vnchelp/badges/master/pipeline.svg)](https://gitlab.com/nobodyinperson/vnchelp/commits/master)
[![deb package](https://img.shields.io/badge/deb-apt-brightgreen.svg)](https://apt.nobodyinperson.de)

# Remote VNC help

`vnchelp` is basically just a little shell script that allows for easy and 
automatic reverse SSH tunnel setup for remote VNC helping purposes.

## When can I use `vnchelp`?

You need a publicly accessible server (e.g. a VPS) with SSH configured for usage
with public key authentication. People you want to support (e.g. family) then 
connect to this server over a reverse SSH connection with `vnchelp` and open a 
port where their VNC server is serving so that you can connect to it.

## Installation

Either install `vnchelp` by adding my 
[apt repository](https://apt.nobodyinperson.de) 
and then running `sudo apt update && sudo apt install vnchelp`, or manually 
download a debian package from the 
[tags page](https://gitlab.com/nobodyinperson/vnchelp/tags) and install it via
`sudo dpkg -i vnchelp*.deb`.